import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Header extends StatelessWidget {
  const Header({
    super.key,
    required this.secondaryColor,
  });

  final Color secondaryColor;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        width: 500,
        decoration: BoxDecoration(
          color: secondaryColor,
          borderRadius: const BorderRadius.vertical(
              top: Radius.circular(10), bottom: Radius.circular(0)),
        ),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            textDirection: TextDirection.ltr,
            children: [
              SvgPicture.asset('lib/assets/images/PokIcon.svg',
                  semanticsLabel: 'Pokedex',
                  excludeFromSemantics: true,
                  matchTextDirection: true,
                  alignment: Alignment.center,
                  height: 25,
                  colorFilter:
                      const ColorFilter.mode(Colors.white, BlendMode.srcIn)),
              const SizedBox(
                width: 5,
              ),
              const Text('POKEDEX',
                  textDirection: TextDirection.ltr,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Roboto'))
            ]));
  }
}
