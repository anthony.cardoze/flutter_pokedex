import 'package:flutter/material.dart';
import 'package:flutter_pokedex/components/description_text.dart';

class Details extends StatelessWidget {
  const Details({
    super.key,
    required this.primaryColor,
  });

  final Color primaryColor;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(50, 15, 50, 15),
        height: 570,
        decoration: BoxDecoration(
          color: primaryColor,
        ),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            textDirection: TextDirection.ltr,
            children: [
              Container(
                  height: 50,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    textDirection: TextDirection.ltr,
                    children: [
                      Image.asset(
                        'lib/assets/images/fire_icon-icons.com_70969.png',
                        semanticLabel: 'Fire',
                        excludeFromSemantics: true,
                        alignment: Alignment.center,
                        height: 30,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'CHARIZARD',
                        textDirection: TextDirection.ltr,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Roboto'),
                      )
                    ],
                  )),
              Stack(
                textDirection: TextDirection.ltr,
                alignment: Alignment.topCenter,
                children: [
                  Positioned(
                    child: Container(
                        padding: const EdgeInsets.fromLTRB(50, 15, 50, 15),
                        margin: const EdgeInsets.fromLTRB(0, 135, 0, 0),
                        height: 350,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          textDirection: TextDirection.ltr,
                          children: const [
                            SizedBox(
                              height: 80,
                            ),
                            DescriptionText(
                              mainText: 'NO.',
                              secondaryText: '006',
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DescriptionText(
                              mainText: 'LEVEL',
                              secondaryText: '100',
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DescriptionText(
                              mainText: 'TYPE',
                              secondaryText: 'FIRE',
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DescriptionText(
                              mainText: 'Ability',
                              secondaryText: 'Blaze',
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DescriptionText(
                              mainText: 'HEIGHT',
                              secondaryText: '1,7m',
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            DescriptionText(
                              mainText: 'WEIGHT',
                              secondaryText: '90,5kg',
                            )
                          ],
                        )),
                  ),
                  Positioned(
                      top: -30,
                      child: ColorFiltered(
                        colorFilter: const ColorFilter.mode(
                            Colors.black, BlendMode.srcIn),
                        child: Image.asset(
                          'lib/assets/images/006.png',
                          semanticLabel: 'Charizard',
                          excludeFromSemantics: true,
                          alignment: Alignment.center,
                          height: 275,
                        ),
                      )),
                ],
              )
            ]));
  }
}
