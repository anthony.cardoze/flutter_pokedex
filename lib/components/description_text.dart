import 'package:flutter/material.dart';

class DescriptionText extends StatelessWidget {
  const DescriptionText({
    super.key,
    required this.mainText,
    required this.secondaryText,
  });

  final String mainText;
  final String secondaryText;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      textDirection: TextDirection.ltr,
      children: [
        Text(mainText.toUpperCase(),
            textDirection: TextDirection.ltr,
            style: const TextStyle(
                color: Colors.black, fontSize: 18, fontFamily: 'Roboto')),
        const Spacer(),
        Text(secondaryText.toUpperCase(),
            textDirection: TextDirection.ltr,
            style: const TextStyle(
                color: Colors.black, fontSize: 18, fontFamily: 'Roboto')),
      ],
    );
  }
}
