import 'package:flutter/material.dart';
import 'package:flutter_pokedex/components/pokemon_box.dart';

class Footer extends StatelessWidget {
  const Footer({
    super.key,
    required this.primaryColor,
  });

  final Color primaryColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(
            top: Radius.circular(10), bottom: Radius.circular(0)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        textDirection: TextDirection.ltr,
        children: [
          const SizedBox(
            height: 10,
          ),
          const Text(
            'OTHERS',
            textDirection: TextDirection.ltr,
            style: TextStyle(
                color: Colors.black, fontSize: 16, fontFamily: 'Roboto'),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            textDirection: TextDirection.ltr,
            children: [
              PokemonBox(
                primaryColor: primaryColor,
                imageURL: 'lib/assets/images/sqBlack.png',
                imageName: 'Squirtle',
              ),
              const SizedBox(
                width: 50,
              ),
              PokemonBox(
                primaryColor: primaryColor,
                imageURL: 'lib/assets/images/Mewtwo.webp',
                imageName: 'Mewtwo',
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            textDirection: TextDirection.ltr,
            children: [
              PokemonBox(
                primaryColor: primaryColor,
                imageURL: 'lib/assets/images/Solgaleo.webp',
                imageName: 'Solgaleo',
              ),
              const SizedBox(
                width: 50,
              ),
              PokemonBox(
                primaryColor: primaryColor,
                imageURL: 'lib/assets/images/snorlax.png',
                imageName: 'Snorlax',
              ),
              
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
