import 'package:flutter/material.dart';

class PokemonBox extends StatelessWidget {
  const PokemonBox({
    super.key,
    required this.primaryColor,
    required this.imageURL,
    required this.imageName,
  });

  final Color primaryColor;
  final String imageURL;
  final String imageName;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          color: primaryColor,
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: ColorFiltered(
          colorFilter: const ColorFilter.mode(Colors.black, BlendMode.srcIn),
          child: Image.asset(
            excludeFromSemantics: true,
            imageURL,
            semanticLabel: imageName,
            matchTextDirection: false,
            alignment: Alignment.center,
          ),
        ));
  }
}
