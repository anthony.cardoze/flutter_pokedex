import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pokedex/components/details.dart';
import 'package:flutter_pokedex/components/header.dart';
import 'package:flutter_pokedex/components/footer.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    const primaryColor = Color(0xffc4c4c4);
    const secondaryColor = Color(0xff777777);
    const whiteColor = Color(0xffe6e6e6);

    return Container(
        decoration: const BoxDecoration(color: primaryColor),
        padding: const EdgeInsets.fromLTRB(14, 60, 14, 50),
        alignment: Alignment.center,
        child: Container(
            width: 500,
            decoration: const BoxDecoration(
              color: whiteColor,
              border: Border(
                top: BorderSide(width: 1.0, color: secondaryColor),
                left: BorderSide(width: 1.0, color: secondaryColor),
                right: BorderSide(width: 1.0, color: secondaryColor),
                bottom: BorderSide(width: 1.0, color: secondaryColor),
              ),
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            child: SingleChildScrollView(
              child: Column(children: const [
                Header(secondaryColor: secondaryColor),
                Details(primaryColor: primaryColor),
                Footer(primaryColor: primaryColor),
              ]),
            )));
  }
}
