
# Ejercicio práctico front-end (Flutter) Pokédex

Tu misión es implementar una interfaz web responsive base de un Pokédex.


## Descripción de la necesidad

La empresa Pokémon necesita implementar el mínimo producto viable de una interfaz responsive de un Pokédex. La figura 1 es un ejemplo gráfico que ilustra los campos y partes principales que debe llevar dicha interfaz. La implementación consta de una sola pantalla estática.

![App Screenshot](https://wiki.pragma.com.co/hs-fs/hubfs/Wiki1.png?width=600&name=Wiki1.png)


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/anthony.cardoze/flutter_pokedex.git
```

Go to the project directory

```bash
  cd flutter_pokedex
```

Start the APP

```bash
  flutter run
```


## Screenshots

![App Screenshot](https://user-content.gitlab-static.net/9e91bb648ac0d5286b963d5d783ac8cbd1e15d5e/68747470733a2f2f692e6962622e636f2f6d7a31427376792f666c75747465722d6170702e706e67)

